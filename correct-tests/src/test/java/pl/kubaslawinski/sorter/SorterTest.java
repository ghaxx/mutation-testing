package pl.kubaslawinski.sorter;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.kubaslawinski.sorter.SortStrategyFactory;
import pl.kubaslawinski.sorter.Sorter;

import java.util.stream.Stream;

import static org.apache.commons.lang.math.RandomUtils.nextInt;
import static org.testng.Assert.*;

public class SorterTest {
    public static final String ARRAYS_DATA_PROVIDER = "arrays";

    @Test(dataProvider = ARRAYS_DATA_PROVIDER)
    public void testSort(Integer[] originalArray) {
        Sorter sorter = new Sorter(originalArray, Sorter.Strategy.BUILT_IN);
        Integer[] array = sorter.sort();

        assertEquals(array.length, originalArray.length, "Arrays differ in size");
        for (int i = 1; i < array.length; i++) {
            assertTrue(array[i - 1] <= array[i], "Numbers are in wrong order");
        }
    }

    @Test(dataProvider = ARRAYS_DATA_PROVIDER)
    public void testQuickSort(Integer[] originalArray) {
        Sorter sorter = new Sorter(originalArray, Sorter.Strategy.QUICK);
        Integer[] array = sorter.sort();

        assertEquals(array.length, originalArray.length, "Arrays differ in size");
        for (int i = 1; i < array.length; i++) {
            assertTrue(array[i - 1] <= array[i], "Numbers are in wrong order");
        }
    }

    @Test(dataProvider = ARRAYS_DATA_PROVIDER)
    public void testBubbleSort(Integer[] originalArray) {
        Sorter sorter = new Sorter(originalArray, Sorter.Strategy.BUBBLE);
        Integer[] array = sorter.sort();

        assertEquals(array.length, originalArray.length, "Arrays differ in size");
        for (int i = 1; i < array.length; i++) {
            assertTrue(array[i - 1] <= array[i], "Numbers are in wrong order");
        }
    }

    @DataProvider(name = ARRAYS_DATA_PROVIDER)
    public static Object[][] arrays() {
        return Stream
                .generate(() -> Stream.generate(() -> nextInt(19) - 9)
                        .limit(nextInt(7) + 5)
                        .toArray(Integer[]::new))
                .limit(4)
                .map((Integer[] array) -> new Object[]{array})
                .toArray(Object[][]::new);
    }

}