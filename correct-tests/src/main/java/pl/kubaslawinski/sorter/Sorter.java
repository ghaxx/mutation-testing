package pl.kubaslawinski.sorter;

import java.util.Arrays;

public class Sorter {
    private Integer[] array;

    public enum Strategy {
        QUICK,
        BUBBLE,
        BUILT_IN
    }

    SortStrategy sortStrategy;

    public Sorter(Integer[] array, Strategy strategy) {
        this.array = array;
        switch (strategy) {
            case QUICK:
                sortStrategy = SortStrategyFactory.quickSortStrategy();
                break;
            case BUBBLE:
                sortStrategy = SortStrategyFactory.bubbleSortStrategy();
                break;
            case BUILT_IN:
                sortStrategy = SortStrategyFactory.builtInSortStrategy();
                break;
        }
    }

    public Integer[] sort() {
        return sortStrategy.sort(array);
    }
}
