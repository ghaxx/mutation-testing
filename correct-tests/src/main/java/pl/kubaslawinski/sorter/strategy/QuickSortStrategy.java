package pl.kubaslawinski.sorter.strategy;

import pl.kubaslawinski.sorter.SortStrategy;

import java.util.Arrays;

public class QuickSortStrategy implements SortStrategy {
    @Override
    public Integer[] sort(Integer[] originalArray) {
        Integer[] array = Arrays.copyOf(originalArray, originalArray.length);
        int p = 0;
        int r = originalArray.length - 1;
        if (p < r) {
            int q = partition(array, p, r);
            quickSort(array, p, q);
            quickSort(array, q + 1, r);
        }
        return array;
    }

    private void quickSort(Integer[] array, int left, int right) {
        if (left < right) {
            int pivot = partition(array, left, right);
            quickSort(array, left, pivot);
            quickSort(array, pivot + 1, right);
        }
    }

    private int partition(Integer[] array, int left, int right) {
        int x = array[left];
        int i = left - 1;
        int j = right + 1;

        while (true) {
            i++;
            while (i < right && array[i] < x)
                i++;
            j--;
            while (j > left && array[j] > x)
                j--;

            if (i < j)
                swap(array, i, j);
            else
                return j;
        }
    }
}
