package pl.kubaslawinski.finder;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.stream.Stream;

import static org.apache.commons.lang.math.RandomUtils.nextInt;

public class FinderTest {

    private static final String CORRECT_DATA_PROVIDER = "CORRECT_DATA_PROVIDER";
    private static final String TOO_LONG_DATA_PROVIDER = "TOO_LONG_DATA_PROVIDER";
    private static final String NOT_TOO_LONG_DATA_PROVIDER = "NOT_TOO_LONG_DATA_PROVIDER";

    @Test(dataProvider = CORRECT_DATA_PROVIDER)
    public void testFind(Integer searched, Integer expectedPosition, Integer[] array) {
        Finder finder = new Finder(array);
        Integer position = finder.find(searched);
        Assert.assertEquals(position, expectedPosition, "Found on wrong position");
    }

    @Test(dataProvider = TOO_LONG_DATA_PROVIDER, expectedExceptions = IllegalArgumentException.class)
    public void testFind_expectException(Integer[] array) {
        Finder finder = new Finder(array);
    }

    @Test(dataProvider = NOT_TOO_LONG_DATA_PROVIDER)
    public void testFindOnLongArrays(Integer[] array) {
        Finder finder = new Finder(array);
    }

    @DataProvider(name = CORRECT_DATA_PROVIDER)
    public static Object[][] arrays() {
        return new Object[][] {
                {1, 0, new Integer[]{1}},
                {1, -1, new Integer[]{2}}
        };
    }

    @DataProvider(name = TOO_LONG_DATA_PROVIDER)
    public static Object[][] tooLongArrays() {
        return new Object[][] {
                {Stream.generate(() -> 0).limit(200).toArray(Integer[]::new)}
        };
    }

    @DataProvider(name = NOT_TOO_LONG_DATA_PROVIDER)
    public static Object[][] notTooLongArrays() {
        return new Object[][] {
                {Stream.generate(() -> 0).limit(50).toArray(Integer[]::new)}
        };
    }
}