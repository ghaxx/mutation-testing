package pl.kubaslawinski.sorter;

public interface SortStrategy {
    Integer[] sort(Integer[] array);

    default void swap(Integer[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
