package pl.kubaslawinski.sorter.strategy;

import pl.kubaslawinski.sorter.SortStrategy;

import java.util.Arrays;

public class BubbleSortStrategy implements SortStrategy {
    @Override
    public Integer[] sort(Integer[] originalArray) {
        Integer[] array = Arrays.copyOf(originalArray, originalArray.length);
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j])
                    swap(array, i, j);
            }
        }

        return array;
    }
}
