package pl.kubaslawinski.sorter.strategy;

import pl.kubaslawinski.sorter.SortStrategy;

import java.util.Arrays;

public class BuiltInSortStrategy implements SortStrategy {

    @Override
    public Integer[] sort(Integer[] originalArray) {
        Integer[] array = Arrays.copyOf(originalArray, originalArray.length);
        Arrays.sort(array);
        return array;
    }
}
