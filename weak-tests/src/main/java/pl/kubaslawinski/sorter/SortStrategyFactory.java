package pl.kubaslawinski.sorter;

import pl.kubaslawinski.sorter.strategy.BubbleSortStrategy;
import pl.kubaslawinski.sorter.strategy.BuiltInSortStrategy;
import pl.kubaslawinski.sorter.strategy.QuickSortStrategy;

public class SortStrategyFactory {
    public static QuickSortStrategy quickSortStrategy() {
        return new QuickSortStrategy();
    }

    public static BubbleSortStrategy bubbleSortStrategy() {
        return new BubbleSortStrategy();
    }

    public static BuiltInSortStrategy builtInSortStrategy() {
        return new BuiltInSortStrategy();
    }
}
