package pl.kubaslawinski.finder;

public class Finder {
    private Integer[] array;

    public Finder(Integer[] array) {
        int arraySizeLimit = 100;
        if (array.length >= arraySizeLimit)
            throw new IllegalArgumentException("Array should have less than 100 elements");
        this.array = array;
    }

    public Integer find(Integer number) {
        int i = 0;
        int length = array.length;
        boolean found = false;

        while (!found && i < length) {
            if (array[i] == number)
                found = true;
            else
                i++;
        }

        if (found)
            return i;
        else
            return -1;
    }
}
